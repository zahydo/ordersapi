# == Route Map
#
#                     Prefix Verb   URI Pattern                                                                              Controller#Action
# v1_contact_info_ubications GET    /api/v1/contact_infos/:contact_info_id/ubications(.:format)                              v1/ubications#index
#                            POST   /api/v1/contact_infos/:contact_info_id/ubications(.:format)                              v1/ubications#create
#               v1_ubication GET    /api/v1/ubications/:id(.:format)                                                         v1/ubications#show
#                            PATCH  /api/v1/ubications/:id(.:format)                                                         v1/ubications#update
#                            PUT    /api/v1/ubications/:id(.:format)                                                         v1/ubications#update
#                            DELETE /api/v1/ubications/:id(.:format)                                                         v1/ubications#destroy
#    v1_person_contact_infos GET    /api/v1/persons/:person_id/contact_infos(.:format)                                       v1/contact_infos#index
#                            POST   /api/v1/persons/:person_id/contact_infos(.:format)                                       v1/contact_infos#create
#            v1_contact_info GET    /api/v1/contact_infos/:id(.:format)                                                      v1/contact_infos#show
#                            PATCH  /api/v1/contact_infos/:id(.:format)                                                      v1/contact_infos#update
#                            PUT    /api/v1/contact_infos/:id(.:format)                                                      v1/contact_infos#update
#                            DELETE /api/v1/contact_infos/:id(.:format)                                                      v1/contact_infos#destroy
#                 v1_persons GET    /api/v1/persons(.:format)                                                                v1/persons#index
#                            POST   /api/v1/persons(.:format)                                                                v1/persons#create
#                  v1_person GET    /api/v1/persons/:id(.:format)                                                            v1/persons#show
#                            PATCH  /api/v1/persons/:id(.:format)                                                            v1/persons#update
#                            PUT    /api/v1/persons/:id(.:format)                                                            v1/persons#update
#                            DELETE /api/v1/persons/:id(.:format)                                                            v1/persons#destroy
#                   v1_users GET    /api/v1/users(.:format)                                                                  v1/users#index
#                            POST   /api/v1/users(.:format)                                                                  v1/users#create
#                    v1_user GET    /api/v1/users/:id(.:format)                                                              v1/users#show
#                            PATCH  /api/v1/users/:id(.:format)                                                              v1/users#update
#                            PUT    /api/v1/users/:id(.:format)                                                              v1/users#update
#                            DELETE /api/v1/users/:id(.:format)                                                              v1/users#destroy
#                   v1_roles GET    /api/v1/roles(.:format)                                                                  v1/roles#index
#                            POST   /api/v1/roles(.:format)                                                                  v1/roles#create
#                    v1_role GET    /api/v1/roles/:id(.:format)                                                              v1/roles#show
#                            PATCH  /api/v1/roles/:id(.:format)                                                              v1/roles#update
#                            PUT    /api/v1/roles/:id(.:format)                                                              v1/roles#update
#                            DELETE /api/v1/roles/:id(.:format)                                                              v1/roles#destroy
#         rails_service_blob GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
#  rails_blob_representation GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#         rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
#  update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#       rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create

Rails.application.routes.draw do
  scope '/api' do
    namespace :v1 do
      resources :persons, shallow: true do
        resources :contact_infos do
          resources :ubications
        end
      end
      resources :users
      resources :roles
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
