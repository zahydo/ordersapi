# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  email      :string
#  password   :string
#  username   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ApplicationRecord
    has_one :role
    validates :email, presence: true, uniqueness: { case_sensitive: false }
    validates :password, presence: true, uniqueness: { case_sensitive: false }
    validates :username, presence: true, uniqueness: { case_sensitive: false }
end
