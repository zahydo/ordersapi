# == Schema Information
#
# Table name: people
#
#  id             :integer          not null, primary key
#  first_name     :string
#  identification :string
#  last_name      :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Person < ApplicationRecord
    has_one :contact_info, dependent: :destroy
    has_one :ubication, through: :contact_info
    
    validates :first_name, presence: true
    validates :identification, presence: true
end
