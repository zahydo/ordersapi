# == Schema Information
#
# Table name: contact_infos
#
#  id         :integer          not null, primary key
#  email      :string
#  telephone  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  person_id  :integer
#
# Indexes
#
#  index_contact_infos_on_person_id  (person_id)
#

class ContactInfo < ApplicationRecord
  belongs_to :person
  has_one :ubication, dependent: :destroy
  validates :telephone, presence: true
end
