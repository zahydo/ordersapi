# == Schema Information
#
# Table name: roles
#
#  id          :integer          not null, primary key
#  description :string
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#
# Indexes
#
#  index_roles_on_user_id  (user_id)
#

class Role < ApplicationRecord
  belongs_to :user
  validates :name, presence: true
end
