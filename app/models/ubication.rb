# == Schema Information
#
# Table name: ubications
#
#  id              :integer          not null, primary key
#  address         :string
#  city            :string
#  neighborhood    :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  contact_info_id :integer
#
# Indexes
#
#  index_ubications_on_contact_info_id  (contact_info_id)
#

class Ubication < ApplicationRecord
  belongs_to :contact_info
  validates :address, presence: true
end
