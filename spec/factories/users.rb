FactoryBot.define do
  factory :user do
    username { Faker::Vehicle.make }
    email { Faker::Internet.email }
    password {Faker::Number.number(10)}
  end
end