FactoryBot.define do
    factory :ubication do
      address { Faker::Address.street_address }
      city { Faker::StarWars.planet }
      neighborhood { Faker::Address.street_name }
      contact_info_id {nil}
    end
  end