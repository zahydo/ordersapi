FactoryBot.define do
  factory :person do
    first_name { Faker::DcComics.hero }
    last_name { Faker::DcComics.name }
    identification { Faker::Number.number(10) }
  end
end