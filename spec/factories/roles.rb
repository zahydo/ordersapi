FactoryBot.define do
  factory :role do
    name { Faker::Gender.type }
    description { Faker::Internet.email }
  end
end