FactoryBot.define do
  factory :contact_info do
    telephone { Faker::Number.number(10) }
    email { Faker::Internet.email }
    person_id {nil}
  end
end