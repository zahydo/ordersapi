require 'rails_helper'

RSpec.describe Person, type: :model do
  # Validate 1-1 Relationship between Person and ContactInfo
  it { should have_one(:contact_info).dependent(:destroy)}
  # Validate presence of first_name 
  it { should validate_presence_of(:first_name)}
  # Validate presence of identification 
  it { should validate_presence_of(:identification)}
end
