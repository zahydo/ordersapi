require 'rails_helper'

RSpec.describe Ubication, type: :model do
  # Validate 1-1 Relationship between Ubication and ContactInfo
  it { should belong_to(:contact_info)}
  # Validate presence of address 
  it { should validate_presence_of(:address)}
end
