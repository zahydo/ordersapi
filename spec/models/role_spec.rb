require 'rails_helper'

RSpec.describe Role, type: :model do
  # Validate 1-1 Relationship between Role and User
  it { should belong_to(:user)}
  # Validate presence of name
  it { should validate_presence_of(:name)}
end
