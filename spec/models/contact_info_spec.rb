require 'rails_helper'

RSpec.describe ContactInfo, type: :model do
  # Validate 1-1 Relationship between Ubication and ContactInfo
  it { should have_one(:ubication).dependent(:destroy)}
  # Validate 1-1 Relationship between Person and ContactInfo
  it { should belong_to(:person)}
  # Validate presence of address 
  it { should validate_presence_of(:telephone)}
end
