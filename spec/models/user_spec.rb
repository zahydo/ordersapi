require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_one(:role)}
  # Validate uniqueness of username 
  it { should validate_presence_of(:username)}
  it { should validate_uniqueness_of(:username).case_insensitive}
  # Validate uniqueness of email 
  it { should validate_presence_of(:email)}
  it { should validate_uniqueness_of(:email).case_insensitive}
  # Validate presence of password
  it { should validate_presence_of(:password)}
end
