class CreateUbications < ActiveRecord::Migration[5.2]
  def change
    create_table :ubications do |t|
      t.string :address
      t.string :city
      t.string :neighborhood
      t.references :contact_info, foreign_key: true

      t.timestamps
    end
  end
end
