class AddPersonRefToContactInfo < ActiveRecord::Migration[5.2]
  def change
    add_reference :contact_infos, :person, foreign_key: true
  end
end
